## 自定义组件

开发工具上。选中目录：右键 新建 components，快速生成小程序目录单个组件目录结构. （注意组件名首字母要大写？

在 相应json文件下声明此目录为组件。
```json
// json 
{
    "component": true,
    // 引入其他组件
    "usingComponents": {
    "Tabs": "../../components/Tabs/Tabs"
  }
}
```

### 组件间数据传递

1. 父组件(页面) 向子组件传递数据，通过标签属性的方式传递对象的副本(对传递而来的数据直接修改，不会反应在父级)
> + 在子组件上进行接受后，直接当作data中的数据访问即可
2. 子向父传递数据，通过自定义事件的方式传递

```html
<!-- 子组件 -->
<view class="tabs" hover-class="none" hover-stop-propagation="false">
    <view class="tabs_title">
        <view class="t_title_item {{item.isActive? 'active': ''}}" wx:for="{{tabs}}" wx:key="id" bindtap="tabsTitleItem" data-index="{{index}}">
            {{item.name}}
        </view>
    </view>
    <view class="tabs_content">内容</view>
</view>
```
```html
<!-- 父组件 -->
<!-- bind + 事件名 监听事件 -->
<Tabs tabs="{{tabs}}" binditemChange="handItemChange"></Tabs>
```

```js
// 子组件
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        // 仅能得到父组件对象的副本
        tabs: {
            type: Array,
            // 默认值
            value:[]
        }

    },

    /**
     * 组件的初始数据
     */
    data: {        
    },

    /**
     * 组件的方法列表
     * 注意：组件(Component)的 事件回调函数是 放在 methods中
     */
    methods: {
        tabsTitleItem(e) {

            let { index } = e.currentTarget.dataset;                
            // 通过自定义事件传递数据
            this.triggerEvent('itemChange', {index})            
        }
    },

    /** 
     * 组件数据字段监听器，用于监听 properties和 data的变化
     */
    observers: {
        'some.subfield': function(subfield) {
            // 使用 setData 设置 this.data.some.subfield 时触发
            // （除此以外，使用 setData 设置 this.data.some 也会触发）
            subfield === this.data.some.subfield
        },        
    }
})
```

```js
// 父组件
Page({
    data: {
        tabs: [{
            id: 0,
            name: "首页",
            isActive: true
        }, {
            id: 2,
            name: "原创",
            isActive: false
        },]
    },

    // 接受子组件传递的数据
    handItemChange(e) {
        let { index } = e.detail
        let { tabs } = this.data

        tabs.forEach((v, i) => {
            i == index ? v.isActive = true : v.isActive = false
        })

        this.setData({
            tabs
        })
    },
})
```

---

## 小程序的生命周期

### 应用生命周期

| 属性 | 说明 |
| :-: | -- | 
| onLaunch | 监听小程序初始化 |
| onShow | 监听小程序启动，或者切前台 |
| onHide | 切后台 |
| onError | 监听错误函数 |
| onPageNotFound | 页面不存在监听函数 |

### 页面生命周期

> ![页面生命周期](https://hcuan.top/api/showimg/293f6642-d612-4fdb-8ec0-5480b436215aSnipaste_2020-11-28_19-23-59.png)
[更多](https://developers.weixin.qq.com/miniprogram/dev/reference/api/Page.html#onAddToFavorites-Object-object)

##### 听说这就是基础部份了

前期的学习内容主要来自 [b站-黑马程序员](https://www.bilibili.com/video/BV1nE41117BQ)

lis

## 小程序的第三方框架
+ 腾讯 wepy
+ 美团 mpvue
+ 京东 taro
+ 滴滴 chameleon
+ uni-app