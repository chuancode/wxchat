## 样式 WXSS

尺寸单位 rpx， 可以根据屏幕宽度自适应。
> 规定屏幕宽度为 750rpx。如iphone6屏幕宽度为 375px, 共有750个物理像素则：`750rpx = 375px = 750物理像素 ---> 1rpx = 0.5px = 1物理像素`

小程序中不需要进行样式文件引入（link），wxml会自动引入同名样式文件。

使用 `@import ""` 语句导入外两样式表，仅支持相对路径

小程序不支持通配符 ‘*’，下列代码无效：
```css
* {
    color: #fff;
}
```

原生小程序不支持 less，但可以通过安装插件实现 vscode -> Easy LESS

## 组件

小程序中常用的布局组件: WeUI :
`view, text, rich-text, button, image, navigator, icon, swiper, radio, checkbox`。 [...](https://developers.weixin.qq.com/miniprogram/dev/component/image.html)

### **text** 文本标签

属性 user-select, space, decode, ...(不可选中，不显示连续空格，不解码)

> decode:（如 `<text>a&nbsp;b</text>` 默认输入`a&nbsp;b`, 加上属性 decode 可获得空格  
> space="": 支持的值 ensp(中文字符空格的一半大小)，emsp(中文空格大小)， nbsp(根据字体
> 设置空格大小)

注意：text组件内仅支持 text嵌套。 除了文本节点意外的其他节点都不能长按选中。

```html
<text class="" user-select space="nbsp" decode>    
    <!--可选中，按照连续显示空格，解码（转义>
</text>
```

---

### **image** 图片组件的默认宽高为 320px-240px, 支持懒加载。默认仅支持网络资源图片

属性 mode , lazy-load  ...（图片的剪裁，缩放的模式。默认scaleToFill(保持比例缩放，完全拉伸至填充盒子。图片懒加载，默认false。

> mode 有13中模式，九种剪裁，4种缩放
![mode模式](https://hcuan.top/api/showimg/e9095135-129a-4b5d-a0ba-744b1a70c523Snipaste_2020-11-27_16-33-58.png)
[更多](https://developers.weixin.qq.com/miniprogram/dev/component/image.html)

> lazy-load 为真时 图片懒加载，图片在即将进入一定范围（上下三屏）时才开始加载

---

### **swiper** 轮播图外层容器，`swiper-item` 每一个轮播项

注意：swiper 默认宽高为 100%, 150px 。无法被子容器撑开。需要自定义宽高

属性：autoplay, interval, circular, indicator-dots （自动轮播，修改轮播时间， 衔接轮播（没有此属性则 1 -2 -3 -2 -1, 面板指示点

--- 

### **navigator** 导航组件，（超链接
  
属性 target url, open-type(跳转至那，默认当前小程序。支持相对，绝对路径。跳转方式)

> open-type 默认值 navigate,保留当前页面，跳转到应用内的某个页面。但是不能跳到 tabbar 页面。
> ![更多](https://hcuan.top/api/showimg/6f611df4-4e09-4229-9441-bb579f945dfdSnipaste_2020-11-27_17-53-00.png)

---

### **rich-text** 富文本标签，可以将字符串解析为对应标签。(并没有完全支持所有html标签)

属性 nodes, space (节点列表或者字符串,通过type区分，默认为元素节点。是否连续显示空格)

```html
<rich-text nodes="{{html}}"></rich-text>

<rich-text nodes="{{html1}}"></rich-text>
```

```js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // 字符串
        html: '<div>hello</div>',
        // 标签列表
        html1: [{
            name: 'div',
            attrs: {
                class: '',
                id: 'html1',
                style: 'color: red;'
            },

            children:[{
                name: 'h1',
                attrs: {},
                children:[{
                    // 文本节点
                    type: 'text',
                    text: 'world'
                }]
            }]
        }]
    },
}
```

---

### **button** 按钮

属性 open-type, （微信开放能力，[微信开放接口？](https://developers.weixin.qq.com/miniprogram/dev/component/button.html)

```html
<!-- button -->
<button>默认按钮</button>
<button size="mini" type="warn">警告按钮</button>
<button size="mini" type="primary" plain="{{true}}">镂空按钮</button>
<button size="mini" loading="{{true}}" disabled>名称前带 loading图标</button>
<!-- c+d 快速选中单词 -->
<!-- open-type属性 -->
<button open-type="contact">打开客服对话功能</button>
<!-- 在我的真机调试上，消息一直发送不出去 -->
<button open-type="share">转发</button>
<!-- 仅支持分享的好友，不支持朋友圈 -->
<button open-type="getPhoneNumber" bindgetphonenumber="getPhone">获取当前用户手机号</button>
<!-- 需要企业权限,通过事件的回调函数获取 -->
<button open-type="getUserInfo" bindgetuserinfo="getUser">获取个人信息</button>
<button open-type="launchApp">打开app</button>
<button open-type="openSetting">打开内置的授权页面</button>
<!-- 管理已经获得的授权 -->
<button open-type="feedback">打开内置 反馈页面</button>
```

```js
Page({
    data: {
        // 字符串
        html: '<div>hello</div>',
        // 标签列表
        html1: [{
            name: 'div',
            attrs: {
                class: '',
                id: 'html1',
                style: 'color: red;'
            },

            children:[{
                name: 'h1',
                attrs: {},
                children:[{
                    // 文本节点
                    type: 'text',
                    text: 'world'
                }]
            }]
        }],        
    },

    /* 获取手机号*/
    getPhone(e) {
        // 获取到的是加密后的 数据，需要在后台服务器中进行解析
        console.log(e)

    },
    // 获取用户信息
    getUser(e) {
        console.log(e)
    },
})
```

---

### **icon** 小程序自带的图标
  
属性
> + type 有效值 `success, success_no_circle, info, warn, waiting, cancel, download, search, clear`	
> + size 默认值 23，
> + color  `'red', 'orange', 'yellow', 'green', 'rgb(0,255,255)', 'blue', 'purple'`

--- 

### **radio** 单选框，需要结合 radio-group一起使用。圆角选框
### **checkbox** 复选框，用法与此相似。

```html
<!-- 单选框 -->
<view >{{changeMsg}}</view>
<radio-group bindchange="handChange">
    <!-- color 选中后 -->
    <radio value="1" color="blue" checked> 男</radio>
    <radio value="0"> 女</radio>
</radio-group>

<!-- 复选框 -->
<view >{{changeItemMsgs}}</view>
<checkbox-group bindchange="handItemChange">
    <checkbox value="{{item.name}}" wx:for="{{changeMsgs}}" wx:key="id">
        {{item.name}}
    </checkbox>
</checkbox-group>
```

```js
Page({
    changeMsg: '',
        changeMsgs: [
            {
                id: 0,
                name: 'apple'
            },
            {
                id: 1,
                name: 'grape'                
            },
            {
                id: 2,
                name: 'banana'
            }
        ],
        changeItemMsgs: []
    },
    handChange(e) {
        console.log(e)
        let changeMsg = e.detail.value * 1 ? '男' : '女'

        this.setData({
            // 等价于 changeMsg: changeMsg            
            changeMsg
        })

    },
    handItemChange(e) {
        console.log(e)
        let changeItemMsgs = e.detail.value

        this.setData({
            changeItemMsgs
        })
    },
})
```