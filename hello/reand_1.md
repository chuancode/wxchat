# 微信小程序

## 小程序的文件结构
+ WXML ---> HTML
+ WXCC ---> CSS
+ javaScript 
+ JSON
> 相较于传统WEB三剑客，微信小程序是四层结构，多了一层 配置.json
​

基本的项目目录的介绍
![](https://hcuan.top/api/showimg/a56cc83a-2ac1-464e-bc1f-cc1f93aebc24dasd7.jpg)

--- 

### app.json  
> 项目全局配置文件。psges {} 页面路径列表, （在官方开发者工具中，指定项目中没有的路径时，会自动生成该目录
```json
{  
      // 注意：配置文件中不允许出现注释，此处仅为演示
    "pages": [
        "pages/main/main", // 目录索引配置，有先后顺序。小程序实体会优先展示第一个页面
        "pages/index/index",
        "pages/img/img",
        "pages/mine/mine",
        "pages/search/search",
        
        "pages/logs/logs"
    ],
    "window": {
        // 全局的默认窗口表现
        "backgroundTextStyle": "dark",
        "navigationBarBackgroundColor": "#4c629a",
        "navigationBarTitleText": "Hcuan",
        "navigationBarTextStyle": "white",
        "enablePullDownRefresh": true,
        "backgroundColor": "#ba9ca6"
    },
    "tabBar": {
        // 底部tab栏的表现
        "list": [{
                "pagePath": "pages/index/index",
                "text": "首页", 
                "iconPath": "icon/home.png", // 图片 路径
                "selectedIconPath": "icon/home_.png" // 此组内容，被选中时的图片路径
            },
            {
                "pagePath": "pages/index/index",
                "text": "图片",
                "iconPath": "icon/png.png",
                "selectedIconPath": "icon/png_.png"
            }        
        ],
        "color": "#0094ff",
        "selectedColor": "ff9400"
    },
    "style": "v2",
    "sitemapLocation": "sitemap.json"
}
```
 [来源](https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/app.html#%E5%85%A8%E5%B1%80%E9%85%8D%E7%BD%AE)

---

### sitemap.json

> 通过配置此文件，决定小程序的某个内容页面是否被微信索引（收录？。没有配置此文件则所有页面允许被索引。

默认是这样的

```json
{
  "desc": "关于本文件的更多信息，请参考文档 https://developers.weixin.qq.com/miniprogram/dev/framework/sitemap.html",
  "rules": [{
  "action": "allow",
  "page": "*"
  }]
}
```

## 模板语法

类似于 vue

**关注点：数据绑定，循环， block标签, 事件绑定**

### 循环
1，直接循环：`wx:for="{{}}" wx:for-item={{遍历得到的列表每一项值}}, wx:for-index={{下表}}, wx:key=""`

2，wx:key 绑定列表中的唯一项，提高列表渲染的性能。可以接受的绑定项：        
> 2.1 字符串 ，字符串的字面值则是，可迭达对象每一项的的唯一属性如：`[{id:1},] wx:key="id"`

> 2.2 wx:key="*this" , 表示对象为一个普通数组. 如：`[1, 2, 3]` 。 `*this` 表示数组本身？？

3, `wx:for-item="", wx-for-index=""`, 如果不写，循环中也会自动生成默认变量：index, item(可以在循环体中直接引用)。这两属性一般用于多层循环嵌套。

### 条件渲染
`wx-if = "{{}}"` 直接从DOM树显示或者隐藏元素

`hidden="{{}}"`  使用样式隐藏或者显示元素

```html
<view>
    <!--数据绑定-->
    <checkbox class="" value="" disabled="false" checked="{{isCheck}}" color=""> 
    是否勾选,此选择框。                        
    </checkbox>      
    <!--循环-->
    <view>
        <view wx:for="{{list}}" wx:for-item="item" wx:for-index="index"
            wx:key="id">
            <view>
                下标：{{index}} -- 值：{{item.name}}
            </view>               
        </view>      
    </view>

    <view><!--block标签，渲染一个结构块，最终不会变为真正的dom元素-->
        <view wx:for="{{list}}" wx:for-item="item" wx:for-index="index"
            wx:key="id">
            <view>
                下标：{{index}} -- 值：{{item.name}}
            </view>                          
        </view>      
    </view>

    <view>
    <view>
        条件渲染
    </view>
        <view wx:if="{{false}}">a1</view><!--渲染显示表达式为真的代码块，方式为表达式为假的代码块直接不存在？-->
        <view wx:elif="{{false}}">a2</view>
        <view wx:else="">a3</view>          
        <view hidden="{{true}}">b</view><!--值为真，不显示。控制显示的方式为 display：none;-->            
    </view>
</view>
```
```js
Page({
    data: {
        msg: 'hello word',
        isCheck: true,
        list: [{id:1, name:'a1'}, {id:2, name:'a2'}],

    },
})
```

### 事件绑定

data中的变量必须在 `this.setDate()`中赋值，外部可以直接引用`this.data.`

小程序中的事件函数不能直接传参，这会被认为是一个函数名`handleInput(1)`, 可以通过自定义属性间接传递参数。


```html
<!-- 事件绑定 -->
<input type="text" bindinput="handleInput" /><!--标签必须闭合-->
<button bindtap="handleTap" data-operation="{{1}}">+</button> <!--无法对小程序的函数 直接传参？， 可以通过自定义属性的方式来传递参数-->
<button bindtap="handleTap" data-operation="{{-1}}">-</button> <!--直接赋值 “1” 为字符串-->
<view>
    {{num}}
</view>
```
```js
Page({
    data: {
        num: 0,
    },
    //输入框 事件监听处理
    handleInput(e) {        
        // 不同于vue.. 变量的赋值须放在 this.setDate()中
        this.setData({
            num: e.detail.value
        })
        console.log(this.data.num) // 但可以直接引用
    },
    // 输入框旁 按钮点击事件
    handleTap(e) {
        console.log(e)
        const temp = e.currentTarget.dataset.operation
        this.setData({
            // * 1 类型转换，对于字符输入则 值为null
            num: this.data.num * 1 + temp
        })
})
```