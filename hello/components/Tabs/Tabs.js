// components/Tabs/Tabs.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        // 仅能得到父组件对象的副本
        tabs: {
            type: Array,
            // 默认值
            value:[]
        }

    },

    /**
     * 组件的初始数据
     */
    data: {        
    },

    /**
     * 组件的方法列表
     * 注意：组件(Component)的 事件回调函数是 放在 methods中
     */
    methods: {
        tabsTitleItem(e) {

            let { index } = e.currentTarget.dataset;                
            // 通过自定义事件传递数据
            this.triggerEvent('itemChange', {index})            
        }
    },
    
})
