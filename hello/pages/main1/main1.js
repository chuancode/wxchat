// pages/main1/main1.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // 字符串
        html: '<div>hello</div>',
        // 标签列表
        html1: [{
            name: 'div',
            attrs: {
                class: '',
                id: 'html1',
                style: 'color: red;'
            },

            children:[{
                name: 'h1',
                attrs: {},
                children:[{
                    // 文本节点
                    type: 'text',
                    text: 'world'
                }]
            }]
        }],
        changeMsg: '',
        changeMsgs: [
            {
                id: 0,
                name: 'apple'
            },
            {
                id: 1,
                name: 'grape'                
            },
            {
                id: 2,
                name: 'banana'
            }
        ],
        changeItemMsgs: []
    },

    /* 获取手机号*/
    getPhone(e) {
        // 获取到的是加密后的 数据，需要在后台服务器中进行解析
        console.log(e)

    },
    // 获取用户信息
    getUser(e) {
        console.log(e)
    },

    handChange(e) {
        console.log(e)
        let changeMsg = e.detail.value * 1 ? '男' : '女'

        this.setData({
            // 等价于 changeMsg: changeMsg            
            changeMsg
        })

    },
    handItemChange(e) {
        console.log(e)
        let changeItemMsgs = e.detail.value

        this.setData({
            changeItemMsgs
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})