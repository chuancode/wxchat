// pages/main/main.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        msg: 'hello word',
        isCheck: true,
        list: [{ id: 1, name: 'a1' }, { id: 2, name: 'a2' }],
        num: 0,
    },
    //输入框 事件监听处理
    handleInput(e) {        
        // 不同于vue.. 变量的赋值须放在 this.setDate()中
        this.setData({
            num: e.detail.value
        })
        console.log(this.data.num) // 但可以直接引用
    },
    // 输入框旁 按钮点击事件
    handleTap(e) {
        console.log(e)
        const temp = e.currentTarget.dataset.operation
        this.setData({
            // * 1 类型转换，对于字符输入则 值为null
            num: this.data.num * 1 + temp
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})