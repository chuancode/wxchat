// pages/category/index.js
import { request } from "../../request/index.js";
Page({

    data: {
        // 左侧的菜单
        leftMenuList: [],
        // 右侧的商品数据
        rightContent: [],
        // 被点击的左侧菜单
        currentIndex: 0,
        // 左侧点击后 右部滚动条位置 归0
        scrollTop: 0,
    },

    // 接口的返回数据, 不放data里是因为他不与渲染层交互，仅临时储存，节省资源
    Cates: [],

    onLoad: function (options) {
        /**
         * 配置缓存
         * 检查本地有没有旧数据
         * 没有旧数据 发送新请求
         * 有旧的数据，同时没有过期，则使用 本地的旧数据
         */
        // 获取本地存储数据
        /** 
         * 于web中数据存储的区别，
         * web localStorage.setItem("key", "value") --> localStorage.getItem("key")
         * 不管存入什么类型的数据，都会调用 toString(), 把数据转换成字符串存入
         * 小程序中：不会进行类型转换        
         */
        const Cates = wx.getStorageSync("cates")
        if (!Cates) {
            this.getCates()
        } else {
            // 定义过期事件
            if (Date.now() - Cates.time > 1000 * 10) {
                this.getCates()
            } else {

                this.Cates = Cates.data

                let leftMenuList = this.Cates.map(v => v.cat_name)
                let rightContent = this.Cates[0].children
                this.setData({
                    rightContent,
                    leftMenuList,
                })
            }
        }
    },

    // 获取分类
    async getCates() {

        // request({
        //     url: "/categories"
        // })
        //     .then(res => {
        //         this.Cates = res.data.message
        //         // 把数据存入本地缓存
        //         wx.setStorageSync('cates', {
        //             time: Date.now(),
        //             data: this.Cates
        //         });

        //         // 构造左侧菜单, map函数把数组中的每一个元素都传入接收的函数。并将函数的返回值组成一个新的数组
        //         let leftMenuList = this.Cates.map(v => v.cat_name)

        //         // 构造右侧商品
        //         let rightContent = this.Cates[0].children
        //         this.setData({
        //             rightContent,
        //             leftMenuList,
        //         })
        //     })

        // 使用 async await 发送异步请求
        const res = await request({ url: "/categories" })
        this.Cates = res.data.message
        // 把数据存入本地缓存
        wx.setStorageSync('cates', {
            time: Date.now(),
            data: this.Cates
        });

        // 构造左侧菜单, map函数把数组中的每一个元素都传入接收的函数。并将函数的返回值组成一个新的数组
        let leftMenuList = this.Cates.map(v => v.cat_name)

        // 构造右侧商品
        let rightContent = this.Cates[0].children
        this.setData({
            rightContent,
            leftMenuList,
        })
    },

    // 左侧菜单的点击事件
    handItemTap(e) {
        const { index } = e.currentTarget.dataset;

        // 构造右侧商品
        let rightContent = this.Cates[index].children
        this.setData({
            currentIndex: index,
            rightContent,
            // 右侧滚动条归0
            scrollTop: 0
        })
    }

})