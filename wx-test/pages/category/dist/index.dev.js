"use strict";

var _index = require("../../request/index.js");

// pages/category/index.js
Page({
  data: {
    // 左侧的菜单
    leftMenuList: [],
    // 右侧的商品数据
    rightContent: []
  },
  // 接口的返回数据, 不放data里是因为他不与渲染层交互，仅临时储存，节省资源
  Cates: [],
  onLoad: function onLoad(options) {
    this.getCates();
  },
  // 获取分类
  getCates: function getCates() {
    var _this = this;

    (0, _index.request)({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/categories"
    }).then(function (res) {
      _this.Cates = res.data.message; // 构造左侧菜单, map函数把数组中的每一个元素都传入接收的函数。并将函数的返回值组成一个新的数组

      var leftMenuList = _this.Cates.map(function (v) {
        return v.cat_name;
      }); // 构造右侧商品


      var rightContent = _this.Cates[0].children;

      _this.setData({
        rightContent: rightContent,
        leftMenuList: leftMenuList
      });
    });
  }
});