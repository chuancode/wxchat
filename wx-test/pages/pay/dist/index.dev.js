"use strict";

var _asyncWx = require("../../utils/asyncWx.js");

/**
 * 页面加载时候， 渲染选中状态 为 check为真的商品
 * 微信支付：仅有企业账号才能调用 支付api。一个appid可以绑定多个开发者
 * 需求 支付
 * 1 支付前判断缓存中是否存在 token, 没有跳转到授权页面获取
 *
 */
Page({
  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    cart: [],
    totalPrice: 0,
    totalNum: 0
  },
  // 监听页面显示
  onShow: function onShow() {
    var address = wx.getStorageSync("address"); // 获取缓存中的购物车数据

    var cart = wx.getStorageSync("cart") || []; // 过滤后的购物车数组

    cart = cart.filter(function (v) {
      return v.checked;
    });
    var totalPrice = 0;
    var totalNum = 0;
    cart.forEach(function (v) {
      totalPrice += v.num * v.goods_price;
      totalNum += v.num;
    });
    this.setData({
      cart: cart,
      totalPrice: totalPrice,
      totalNum: totalNum,
      address: address
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function onLoad(options) {},
  // 点击支付
  handOrderPay: function handOrderPay() {
    // 1 判断缓存中是否有 token
    var token = wx.getStorageSync("token");

    if (!token) {
      wx.navigateTo({
        url: "/pages/auth/index"
      });
      return;
    }

    console.log("存在token");
  }
});