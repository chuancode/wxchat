/**
 * 页面加载时候， 渲染选中状态 为 check为真的商品
 * 微信支付：仅有企业账号才能调用 支付api。一个appid可以绑定多个开发者
 * 需求 支付
 * 1 支付前判断缓存中是否存在 token, 没有跳转到授权页面获取
 * 2 支付成功，删除已经支付过的商品
 */

import { requestPayment, showToast } from "../../utils/asyncWx.js"
import { request } from "../../request/index.js"
Page({
    /**
     * 页面的初始数据
     */
    data: {
        address: {},
        cart: [],
        totalPrice: 0,
        totalNum: 0,
    },

    // 监听页面显示
    onShow() {
        const address = wx.getStorageSync("address")
        // 获取缓存中的购物车数据
        let cart = wx.getStorageSync("cart") || []
        // 过滤后的购物车数组
        cart = cart.filter((v) => v.checked)

        let totalPrice = 0
        let totalNum = 0
        cart.forEach((v) => {
            totalPrice += v.num * v.goods_price
            totalNum += v.num
        })

        this.setData({
            cart,
            totalPrice,
            totalNum,
            address,
        })
    },

    // 点击支付
    async handOrderPay() {
        try {
            // 1 判断缓存中是否有 token
            const token = wx.getStorageSync("token")
            if (!token) {
                wx.navigateTo({
                    url: "/pages/auth/index",
                })
                return
            }
            // 准备请求头， 获取订单编号
            // 请求体
            const order_price = this.data.totalPrice
            const consignee_addr = this.data.address.all
            const cart = this.data.cart
            let goods = []
            cart.forEach((v) => {
                goods_id: v.goods_id
                goods_number: v.num
                goods_price: v.goods_price
            })
            const orderParams = { order_price, consignee_addr, goods }
            // 创建订单
            const { order_number } = (
                await request({
                    url: "/my/orders/create",
                    method: "POST",
                    data: orderParams,
                })
            ).data.message

            // 准备预支付，获得支付参数
            const { pay } = (
                await request({
                    url: "/my/orders/req_unifiedorder",
                    method: "POST",
                    data: { order_number },
                })
            ).data.message

            // 进行支付，，没有权限
            // await requestPayment(pay)

            // 查询后台，订单状态, 未支付
            const res = (
                await request({
                    url: "/my/orders/chkOrder",
                    method: "POST",
                    data: { order_number },
                })
            ).data.message

            await showToast({ title: "支付成功" })

            // 删除已经删除过的商品
            let newCart = wx.getStorageSync("cart")
            newCart = newCart.filter((v) => !v.checked)
            wx.setStorageSync("cart", newCart)

            // 成功跳转到 订单页面
            wx.navigateTo({
                url: "/pages/order/index",
            })
        } catch (error) {
            await showToast({ title: "支付失败" })
            console.log(error)
        }
    },
})
