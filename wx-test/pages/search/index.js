/**
 * 监听搜索框值的改变进行搜索
 * 防抖，节流
 */

import { request } from "../../request/index.js"
Page({
    data: {
        goods: [],
        // 取消按钮，显示
        isFocus: false,
        // 输入框的值
        inputValue: "",
    },
    TimeId: -1,

    // 监听输入框
    handInput(e) {
        // 获取输入值
        const { value } = e.detail
        if (!value.trim()) {
            this.setData({
                goods: [],
                // 取消按钮，显示
                isFocus: false,
            })
            // 此处不清楚定时器，则上次请求中的换返回值会覆盖此处清空的 数组
            clearTimeout(this.TimeId)
            return
        }

        this.setData({
            isFocus: true,
        })

        // 准备1发送请求
        // 清楚定时器,-hwei-> 第一次进入函数，h进入定时器，1秒后执行，二次 w清楚上次定时器，执行 hw..， 第三次...
        clearTimeout(this.TimeId)
        this.TimeId = setTimeout(() => {
            this.qSearch(value)
        }, 1000)
    },
    // 获取搜索结果
    async qSearch(query) {
        const res = await request({ url: "/goods/qsearch", data: { query } })
        this.setData({
            goods: res.data.message,
        })
    },
    // 点击取消
    handCancel() { 
        this.setData({
            goods: [],
            // 取消按钮，显示
            isFocus: false,
            // 输入框的值
            inputValue: "",
        })
    },
})
