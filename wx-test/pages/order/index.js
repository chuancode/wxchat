/**
 * 需求
 * 页面打开时，获取type参数，根据参数发送请求获取订单数据。
 * * 根据type 决定那个值被选中
 * 点击不同标题，重新发送请求，获取相应数据
 * 注意判断缓存中是否有token
 */
import { request } from "../../request/index.js"
Page({
    /**
     * 页面的初始数据
     */
    data: {
        orders: [],
        tabs: [
            {
                id: 0,
                value: "全部",
                isActive: true,
            },
            {
                id: 1,
                value: "待付款",
                isActive: false,
            },
            {
                id: 2,
                value: "待发货",
                isActive: false,
            },
            {
                id: 3,
                value: "退款/退货",
                isActive: false,
            },
        ],
    },

    onShow() {
        const token = wx.getStorageSync("token")
        if (!token) {
            wx.navigateTo({
                url: "/pages/auth/index",
            })
            return
        }

        // on show 无法接受路由中的参数 onLoad可以。其他方法
        // 获取小程序的页面 栈-数组， 长度最大是 10页面
        let pages = getCurrentPages()
        // 数组最后一个 页面，就是当前页面
        let { type } = pages[pages.length - 1].options
        // 激活选中页面标题
        this.changeTitleByIndex(type - 1)
        // 获取相应订单
        this.getOrders(type)
    },

    // 根据标题索引 决定被选中的标题数组
    changeTitleByIndex(index) {
        let { tabs } = this.data
        tabs.forEach((v, i) => {
            i === index ? (v.isActive = true) : (v.isActive = false)
        })
        this.setData({
            tabs,
        })
    },

    // 子组件标题点击事件,修改分类筛选选中
    handTabsItemChange(e) {
        const { index } = e.detail
        this.changeTitleByIndex(index)
        // 重新发送请求获取数据
        this.getOrders(index + 1)
    },

    // 获取订单列表
    async getOrders(type) {
        const res = await request({ url: "/my/orders/all", data: { type } })
        this.setData({
            orders: res.data.message.orders.map((v) => ({
                ...v,
                create_time_cn: new Date(v.create_time * 1000).toLocaleString(),
            })),
        })
    },

    onLoad(e) {},
})
