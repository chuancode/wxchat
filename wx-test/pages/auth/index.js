// pages/auth/index.js
import { login } from "../../utils/asyncWx.js"
import { request } from "../../request/index.js"
Page({
    async handGetUserInfo(e) {
        try {
            // 获取用户信息, 设置token
            const { rawData, encryptedData, signature, iv } = e.detail
            // 获取登录后的 code
            const { code } = await login()
            const loginParams = { rawData, encryptedData, signature, iv, code }

            // const {token} = await request({ url: "/users/wxlogin", data: loginParams, method: "post" })
            // 没有权限无法获取， 假装获取成功
            const token =
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo"

            wx.setStorageSync("token", token)
            wx.navigateBack({
                delta: 1,
            })
        } catch (err) {
            console.log(err)
        }
    },
})
