/**
 * 1 点击事件获取收获地址。调用api获取. 获取后 存入本地储存
 * api  wx.chooseAddress 用户拒绝，之后就不能在调用
 * 真机调试现在不会出现此情况，（对此微信官方 getSetting 接口返回的状态页一直为 true，以兼容之前代码
 * 解决方式：
 * * 获取用户对小程序的 所获取的权限状态 scope
 * * 假如用户确定， scope的值 为 true authSetting
 * * 用户 拒绝 scope为假
 * * 用户没有调用过此接口， scope =>undefined
 * 2 页面显示时，获取缓存的地址数据，有则显示，没有则 显示按钮获取
 * 3 页面显示时 onshow, 获取缓存中的购物车数组，填充到data中
 * 4 全选，商品全部被选中则全选按钮也被选中，点击全选所有商品 checked=true
 * 5 总价格和总数量，都需要商品被选中
 * 6 单个商品选中事件 ， --商品的选中状态取反，checked, 重新计算总价格数量
 * 7 全选点击事件，下边栏数据，data绑定的全选状态值取反。将购物车数据所有选中置为 全选的状态值. 修改下边数据
 * 8 通过点击事件的自定义参数（+ - id，完成对 商品数量的编辑, 当商品数量为 1 时，用户点击 -， 弹窗是否删除商品
 * - 弹窗 api 。showModel
 * 9 结算事件， 判断地址信息，有没有商品。成功跳转到支付
 */

import { chooseAddress, showModel, showToast } from "../../utils/asyncWx.js"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        address: {},
        cart: [],
        allChecked: false,
        totalPrice: 0,
        totalNum: 0
    },

    // 监听页面显示
    onShow() {
        const address = wx.getStorageSync("address");
        // 获取缓存中的购物车数据
        const cart = wx.getStorageSync("cart") || [];
        this.setData({
            address
        })
        this.setCart(cart)

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    // 点击收货地址按钮 
    async handChooseAddress() {
        // 用户取消授权 错误管理
        try {
            let address = await chooseAddress()
            address.all = address.provinceName + address.cityName + address.countyName + address.detailInfo
            wx.setStorageSync("address", address);
        } catch (err) {
            console.log(err);
        }
    },
    // 商品选中
    handItemChange(e) {
        const goods_id = e.currentTarget.dataset.id;
        let { cart } = this.data;
        let index = cart.findIndex(v => v.goods_id === goods_id);
        cart[index].checked = !cart[index].checked;

        this.setCart(cart)
    },

    // 计算底部工具栏数据，存入最新 购物车数据到缓存
    setCart(cart) {

        // 计算全选, every 接受一个回调函数，对数组进行遍历，每次的结果为真，则every 返回 真
        // 如果空数组调用 every,返回 true
        // const allChecked = cart.length ? cart.every(v => v.checked) : false;

        // 总价格，总数量
        let totalPrice = 0;
        let totalNum = 0;
        let allChecked = true
        cart.forEach(v => {
            if (v.checked) {
                totalPrice += v.num * v.goods_price;
                totalNum += v.num;
            } else {
                allChecked = false;
            }
        })
        // 解决数组为空问题
        allChecked = cart.length != 0 ? allChecked : false;
        this.setData({
            cart,
            allChecked,
            totalPrice,
            totalNum
        })

        wx.setStorageSync("cart", cart)
    },
    // 商品全选事件
    handItemAllChange() {
        let { cart, allChecked } = this.data;
        allChecked = !allChecked;

        cart.forEach(v => v.checked = allChecked);

        this.setCart(cart)

    },
    // 单个商品 +-
    async handItemNumEdit(e) {
        const { operation, id } = e.currentTarget.dataset;
        const { cart } = this.data;
        const index = cart.findIndex(v => v.goods_id === id);
        // 判断是否执行删除
        if (cart[index].num === 1 && operation === -1) {            
            const res = await showModel({ content: '你是否要删除此商品'});
            if (res.confirm) {
                cart.splice(index, 1)
                this.setCart(cart)
            } else {
                console.log('exit');
            }
        } else {
            cart[index].num += operation;
            this.setCart(cart);
        }
    },
    // 结算
    async handPay(){
        const {address, totalNum} = this.data;
        if (!address.userName){
            await showToast({title: '你还没有选择收货地址'})
            return;
        }
        if(! totalNum){
            await showToast({title: '你还没有选择选购商品'})
            return;
        }
        // 跳转到支付
        wx.navigateTo({
            url: '/pages/pay/index',
        });
          
    }
})