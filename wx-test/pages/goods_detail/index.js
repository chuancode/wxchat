import { request } from "../../request/index.js"
/**
 * 需求获取商品详情
 * 需求，点击轮播图 预览大图
 * 图片点击事件调动 api,
 * 需求，点击加入购物车，
 * 点击事件，已存在当前商品，则数量1，不存在则添加商品（添加购买数量属性）到缓存
 * 需求 商品收藏
 * 页面onshow的时候，加载缓存中·的商品收藏的数据
 * 是 ； 改变页面的图标，
 * 点击收藏按钮时，判断缓存中是否有此商品，存在 取消收藏
 */
Page({
    /**
     * 页面的初始数据
     */
    data: {
        goodsObj: {},
        // 商品是否被收藏
        isCollect: false,
    },
    // 预览轮播图数据
    GoodsInfo: {},
    /**
     * 生命周期函数--监听页面加载
     */
    onShow: function () {
        let pages = getCurrentPages()
        let currentPage = pages[pages.length - 1]
        let { options } = currentPage
        const { goods_id } = options
        this.getGoodsDetail(goods_id)
    },

    // 获取商品详情数据
    async getGoodsDetail(goods_id) {
        let res = await request({ url: "/goods/detail", data: { goods_id } })
        res = res.data.message
        this.GoodsInfo = res
        // 获取缓存中的商品收藏数组
        let collect = wx.getStorageSync("collect") || []
        // 回调函数中有一个返回真，则函数返回真
        let isCollect = collect.some((v) => v.goods_id == goods_id)
        this.setData({
            isCollect,
            goodsObj: {
                goods_name: res.goods_name,
                goods_price: res.goods_price,
                // iphone 不支持 webp图片格式
                // 最好让后端修改，他是个人
                // 确保后端有 同名的 jpg文件格式
                goods_introduce: res.goods_introduce.replace(/\.webp/g, ".jpg"),
                pics: res.pics,
            },
        })
    },

    // 点击轮播图 放大预览
    handPreviewImage(e) {
        const urls = this.GoodsInfo.pics.map((v) => v.pics_mid)
        wx.previewImage({
            // 得到点击的那张图片地址
            current: e.currentTarget.dataset.url,
            urls: urls,
        })
    },
    // 加入购物车
    handCartAdd() {
        // 1 获取 缓存中的购物车数组
        let cart = wx.getStorageSync("cart") || []
        // 判断是否有此商品
        let index = cart.findIndex((v) => v.goods_id === this.GoodsInfo.goods_id)
        if (index === -1) {
            this.GoodsInfo.num = 1
            // 加入购物车默认被选中
            this.GoodsInfo.checked = true
            cart.push(this.GoodsInfo)
        } else {
            cart[index].num++
        }

        // 把修改后的购物车数组，重新加入缓存
        wx.setStorageSync("cart", cart)
        // 提示用户
        wx.showToast({
            title: "加入成功",
            icon: "success",
            image: "",
            duration: 1500,
            // 阻止用户多次点击，mask 成立用户1.5秒后点击屏幕才会生效
            mask: true,
        })
    },
    // 点击收藏图标
    handCollect() {
        let isCollect = false
        // 获取缓存商品收藏
        let collect = wx.getStorageSync("collect") || []
        let index = collect.findIndex((v) => v.goods_id === this.GoodsInfo.goods_id)
        if (index !== -1) {
            collect.splice(index, 1)
            wx.showToast({
                title: "取消成功",
                icon: "success",
                duration: 500,
                mask: true,
            })
        } else {
            isCollect = true
            collect.push(this.GoodsInfo)
            wx.showToast({
                title: "收藏成功",
                icon: "success",
                duration: 500,
                mask: true,
            })
        }
        wx.setStorageSync("collect", collect)
        this.setData({
            isCollect,
        })
    },
})
