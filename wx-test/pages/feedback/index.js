/**
 * 需求， 点击加号触发小程序内置的 选择图片qpi
 * 获取到图片路径，存入data.并在 UpImg 显示
 * * 需求，点击图片，删除索引中对应的元素
 * * 提交按钮的 操作步骤
 * 对文本内容的 合法性验证，
 * 将用户选择的图片上传到专门的图片服务器，返回外网图片的链接
 * 将文本域 和 外网的图片路径一起提交到 服务器
 * 清空当前页面，返回上一页
 */
Page({
    /**
     * 页面的初始数据
     */
    data: {
        tabs: [
            {
                id: 0,
                value: "体验问题",
                isActive: true,
            },
            {
                id: 1,
                value: "商品/商家投诉",
                isActive: false,
            },
        ],
        // 选中图片的路径数组
        chooseImgs: [],
        // 文本域内容
        textValue: "",
    },
    // 外网图片路径数组
    upLoadImgs: [],

    // 子组件标题点击事件,修改分类筛选选中
    handTabsItemChange(e) {
        const { index } = e.detail
        let { tabs } = this.data
        tabs.forEach((v, i) => {
            i === index ? (v.isActive = true) : (v.isActive = false)
        })
        this.setData({
            tabs,
        })
    },
    // 点击加号选择图片
    handChooseImg() {
        wx.chooseImage({
            // 最多同时选中的图片数量
            count: 9,
            // 图片格式，原图，压缩
            sizeType: ["original", "compressed"],
            // 图片来源，相册 照相机
            sourceType: ["album", "camera"],
            success: (result) => {
                this.setData({
                    // 图片数组拼接
                    chooseImgs: [...this.data.chooseImgs, ...result.tempFilePaths],
                })
            },
        })
    },
    // 点击删除图片
    handRemoveImg(e) {
        const { index } = e.currentTarget.dataset
        let { chooseImgs } = this.data

        chooseImgs.splice(index, 1)
        this.setData({
            chooseImgs,
        })
    },
    // 文本域的输入事件
    handTextInput(e) {
        this.setData({
            textValue: e.detail.value,
        })
    },
    // 提交按钮
    handFormSubmit() {
        const { textValue, chooseImgs } = this.data
        // 效验合法
        if (!textValue) {
            wx.showToast({
                title: "输入不合法",
                icon: "none",
                mask: true,
            })
            return
        }
        // 准备上传图片值 专门的服务器。 不支持同时多个上传
        // 显示正在等待的图片
        wx.showLoading({
            title: "正在上传中",
            mask: true,
        })
        // 判断是否有图片
        if (chooseImgs.length != 0) {
            chooseImgs.forEach((v, i) => {
                wx.uploadFile({
                    url: "https://images.ac.cn/Home/Index/UploadAction/",
                    // 图片本地地址
                    filePath: v,
                    // 表单项名
                    name: "file",
                    // 额外文本信息
                    formData: {},
                    success: (result) => {
                        this.upLoadImgs.push("没有相应上传接口")
                        if (i === chooseImgs.length - 1) {
                            console.log("把文本域内容，和外网的数据，提交到后台，也没有相应接口")
                            // 重置页面
                            this.setData({
                                textValue: "",
                                chooseImgs: [],
                            })
                        }
                    },
                })
            })
        } else {
            console.log("只是提交了文本")
        }
        wx.hideLoading()
        // 返回上一个页面
        wx.navigateBack({
            delta: 1,
        })
    },
})
