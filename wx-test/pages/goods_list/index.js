/**
 * 需求 滚动条触底 开始加载下一页
 * 1 滚动条触底 判断是否有下一页，没右 提示
 * 需求 下拉刷新
 * 触发下拉事件，页面json开启配置项 
 * 重置商品数组，重置页码
 * 关闭接口配置的等待效果及下拉框
 */
import { request } from "../../request/index.js";
Page({

    /**
     * 页面的初始数据
     */
    data: {
        tabs: [
            {
                id: 0,
                value: "综合",
                isActive: true,
            },
            {
                id: 1,
                value: "销量",
                isActive: false,
            },
            {
                id: 2,
                value: "价格",
                isActive: false,
            }
        ],
        goodsList: []
    },

    // 接口需要的参数
    QueryParams: {
        query: '',
        cid: '',
        pagenum: 1,
        pagesize: 7
    },
    // 接口的总页数
    totalPages: 1,

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // 获取路由信息
        this.QueryParams.cid = options.cid
        this.getGoodsList();
            
    },

    // 子组件标题点击事件,修改分类筛选选中
    handTabsItemChange(e) {
        const { index } = e.detail
        let { tabs } = this.data;
        tabs.forEach((v, i) => { i === index ? v.isActive = true : v.isActive = false })
        this.setData({
            tabs
        })
    },

    // 获取商品列表
    async getGoodsList() {
        const res = await request({
            url: '/goods/search',
            data: this.QueryParams
        })
        // 获取商品总条数
        const total = res.data.message.total
        this.totalPages = Math.ceil(total / this.QueryParams.pagesize)

        this.setData({
            // 数组拼接
            goodsList: [...this.data.goodsList, ...res.data.message.goods]
        })

        // 关闭下拉刷新的窗口
        wx.stopPullDownRefresh()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        // 滚动条触底，加载下页商品
        if (this.QueryParams.pagenum >= this.totalPages) {
            // 无下一页
            wx.showToast({
                title: '没有下一页了呀！',
            });
        } else {
            this.QueryParams.pagenum++;
            this.getGoodsList()
        }
    },
    // 下拉刷新事件
    onPullDownRefresh() {
        // 重置数据
        this.setData({
            goodsList: []
        })
        this.QueryParams.pagenum = 1
        this.getGoodsList()
    }
})