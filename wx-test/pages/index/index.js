//Page Object
// 引入用来发送请求的 方法
import { request } from "../../request/index.js";
Page({
    data: {
        // 轮播图
        swiperList: [],
        // 分类导航图
        catesList: [],
        // 楼层
        floorList: [],

    },
    //options(Object)
    onLoad: function (options) {
        // 多层嵌套请求，优化。es6.promise,这也是一个一定要学习的东西
        this.getSwiperList()
        this.getCatesList()
        this.getFloorList()
    },

    // 获取轮播图数据
    getSwiperList() {
        // wx.request({
        //     url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
        //     data: {},
        //     header: {'content-type':'application/json'},
        //     method: 'GET',
        //     dataType: 'json',
        //     responseType: 'text',
        //     success: (result)=>{
        //         this.setData({
        //             swiperList : result.data.message
        //         })
        //     },
        //     fail: ()=>{},
        //     complete: ()=>{}
        // });

        request({ url: '/home/swiperdata' })
            .then(result => {
                this.setData({
                    swiperList: result.data.message
                })
            })
    },
    // 获取分类导航
    getCatesList() {
        request({ url: '/home/catitems' })
            .then(result => {
                this.setData({
                    catesList: result.data.message
                })
            })
    },
    // 获取商品楼层
    getFloorList() {
        request({ url: '/home/floordata' })
            .then(result => {
                this.setData({
                    floorList: result.data.message
                })
            })
    },    
});