// 同时发送异步请求的次数,因为同步发送请求，而非同步关闭加载框，可能会在第一个返回时就进行关闭。而 二三请求还没有完成
let ajaxTimes = 0

// 自定义请求接口 Promise
export const request = (params) => {
    // 判断请求的路由是否需要 header token,
    // 对原有的token 进行添加
    let header = {...params.header}
    if (params.url.includes("/my/")) {
        header["Authorization"] = wx.getStorageSync("token")
    }

    ajaxTimes++
    // 公共url
    const baseUrl = "https://api-hmugo-web.itheima.net/api/public/v1"
    // 加载效果
    wx.showLoading({
        title: "加载中",
        mask: true,
    })

    return new Promise((resolve, reject) => {
        wx.request({
            ...params,
            header: header,
            url: baseUrl + params.url,
            success: (result) => {
                resolve(result)
            },
            fail: (err) => {
                reject(err)
            },
            complete: () => {
                if (--ajaxTimes === 0) {
                    wx.hideLoading()
                }
            },
        })
    })
}
