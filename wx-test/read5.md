## 防抖，一般用于 输入框中，防止重复输入，重复发送请求

> 在一个实时发送请求的搜索框中，希望输入稳定（暂停输入）后才发送请求（定时器实现

#### setTimeout(f, t)

> setTimeout 函数用来指定某个函数或某段代码，在 t 毫秒之后执行。它返回一个整数，表示定时器的编号，以后可以用 `clearTimeout(id)`取消未执行的调用。

setTimeout 还允许添加更多的参数。它们将被传入推迟执行的函数（回调函数）。IE 9.0 及以下版本，只允许 setTimeout 有两个参数，不支持更多的参数;

`setTimeout((v)={}, 1000, 1)`

setTimeout 推迟执行的回调函数如果是某个对象的方法，那么该方法中的 this 关键字将指向全局环境，而不是定义时所在的那个对象。

```js
var x = 1

var o = {
    x: 2,
    y: function () {
        console.log(this.x)
    },
}
setTimeout(o.y, 1000) // > 1
```

---

#### setInterval(f, t)

> 指定代码按照 t 为一个周期，多次执行。也会返回定时器编号。用 `clearInterval(id)`, 取消未执行的调用。**在不加干涉的情况下，间歇调用将会一直执行到页面卸载。**


**注意：**

JS 是一个单线程的解释器，因此一段时间内只能执行一段代码。为了要控制执行的代码，就有一个 JS 任务队列。这些任务会按照将它们添加到队列的顺序执行。

> 定时器的 t 告诉 js 再过多久把当前任务添加到队列中，如果队列是空，那么添加的代码会立即执行；如果队列不空，那么它就要等前面的代码执行完了以后在执行。

> 当一个定时器的回调函数执行时间特别长，以致于执行过程中当前计时器已经添加了多个 计时器的回调函数，则当前任务队列中的第 3 个 回调函数，会被抛弃。**任务队列中不会有两个及以上同一个定时器的回调函数**

--- 

**防抖实现**

```js
// 监听输入框值改变事件
pages({
    // 定时器 id
    timeId = -1,

    handleInput(e){
        // 获取输入值
        const {val} = e.detail

        // 清除之前的 定时器
        clearTimeout(this.timeId)

        this.TimeId = setTimeout(() => {
            this.qSearch(val)
        }, 1000)
    },

    qSearch(val){
        // request
    }
})
```




参考资料：

[setTimeout 和 setInterval](https://www.jianshu.com/p/fc9a08ca2c92)

[你所不知道的 setInterval](https://www.jianshu.com/p/894667dee1aa)

[节流](https://www.cnblogs.com/momo798/p/9177767.html)

原生提交测试