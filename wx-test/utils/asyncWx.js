/* 获取收获地址, 封装为 Promise 形式 */
export const chooseAddress = () => {
    return new Promise((resolve, reject) => {
        wx.chooseAddress({
            success: (result) => {
                resolve(result)
            },
            fail: (err) => {
                reject(err)
            },
        })
    })
}

/* 弹窗提示 */
export const showModel = ({ content }) => {
    return new Promise((resolve, reject) => {
        wx.showModal({
            title: "提示",
            content: content,
            cancelText: "取消",
            cancelColor: "#000000",
            confirmText: "确定",
            confirmColor: "red",
            success: (res) => {
                resolve(res)
            },
            fail: (err) => {
                resolve(err)
            },
        })
    })
}

/* 弹窗提示 */
/* 弹窗提示 */
export const showToast = ({ title }) => {
    return new Promise((resolve, reject) => {
        wx.showToast({
            title: title,
            icon: "none",
            success: (res) => {
                resolve(res)
            },
            fail: (err) => {
                resolve(err)
            },
        })
    })
}

/* 获取登录信息 */
export const login = () => {
    return new Promise((resolve, reject) => {
        wx.login({
            timeout: 10000,
            success: (result) => {
                resolve(result)
            },
            fail: (err) => {
                reject(err)
            },
        })
    })
}

/**
 * param 形式封装的 小程序支付接口
 * @param {object} pay 支付所必须需要的参数
 */
export const requestPayment = (pay) => {
    return new Promise((resolve, reject) => {
        wx.requestPayment({
            ...pay,
            success: (result) => {
                resolve(result)
            },
            fail: (err) => {
                reject(err)
            },
        })
    })
}
