"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.login = exports.showToast = exports.showModel = exports.chooseAddress = void 0;

/* 获取收获地址, 封装为 Promise 形式 */
var chooseAddress = function chooseAddress() {
  return new Promise(function (resolve, reject) {
    wx.chooseAddress({
      success: function success(result) {
        resolve(result);
      },
      fail: function fail(err) {
        reject(err);
      }
    });
  });
};
/* 弹窗提示 */


exports.chooseAddress = chooseAddress;

var showModel = function showModel(_ref) {
  var content = _ref.content;
  return new Promise(function (resolve, reject) {
    wx.showModal({
      title: "提示",
      content: content,
      cancelText: "取消",
      cancelColor: "#000000",
      confirmText: "确定",
      confirmColor: "red",
      success: function success(res) {
        resolve(res);
      },
      fail: function fail(err) {
        resolve(err);
      }
    });
  });
};
/* 弹窗提示 */

/* 弹窗提示 */


exports.showModel = showModel;

var showToast = function showToast(_ref2) {
  var title = _ref2.title;
  return new Promise(function (resolve, reject) {
    wx.showToast({
      title: title,
      icon: "none",
      success: function success(res) {
        resolve(res);
      },
      fail: function fail(err) {
        resolve(err);
      }
    });
  });
};
/* 获取登录信息 */


exports.showToast = showToast;

var login = function login() {
  return new Promise(function (resolve, reject) {
    wx.login({
      timeout: 10000,
      success: function success(result) {
        resolve(result);
      },
      fail: function fail(err) {
        reject(err);
      }
    });
  });
};

exports.login = login;