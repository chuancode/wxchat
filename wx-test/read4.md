
## async，让异步编程更简单
> async和 await关键字是最近添加到 JavaScript语言里面的。它们是ECMAScript 2017 JavaScript版的一部分（参见ECMAScript Next support in Mozilla）。简单来说，它们是基于promises的语法糖，使异步代码更易于编写和阅读。通过使用它们，异步代码看起来更像是老式同步代码.

> async 函数也可以说是 Generator 函数的语法糖。

相较于 Generator函数：
1. 内置执行器。 Generator函数的执行必须靠执行器，所以才有了 co函数库，而 async函数自带执行器。
2. 更好的语义。 async和 await，比起星号和 yield，语义更清楚了。async表示函数里有异步操作，await 表示紧跟在后面的表达式需要等待结果。
3. 更广的适用性。 co函数库约定，yield命令后面只能是 Thunk函数或 Promise对象，而 async函数的 await命令后面，可以跟 Promise对象和原始类型的值（数值、字符串和布尔值，但这时等同于同步操作）。

### async 

使用 **async**关键字，把它放在函数声明之前，使其成为 async function。异步函数是一个知道怎样使用 await关键字调用异步代码的函数。

`async function hello() { return "Hello" };` 

调用该函数会返回一个 promise对象，这是异步函数的特征之一  它保证函数的返回值为 promise。

获得他的返回值：

`hello().then((value) => console.log(value))`

async 关键字加到函数申明中，可以告诉它们返回的是 promise，而不是直接返回值。此外，它避免了同步函数为支持使用 await带来的任何潜在开销。在函数声明为 async时，JavaScript引擎会添加必要的处理，以优化你的程序。

---

### await

当 await 关键字与异步函数一起使用时，它的真正优势就变得明显了 —— 事实上， await只在异步函数里面才起作用。它可以放在任何异步的，基于 promise的函数之前。它会暂停代码在该行上，直到 promise完成，然后返回结果值。在暂停的同时，其他正在等待执行的代码就有机会执行了。

**await关键字仅在 async function中**有效。如果在 async function函数体外使用 await，你只会得到一个语法错误。

```js
async function hello() {
    return greeting = await Promise.resolve("Hello");
};

hello().then(alert);
```

### 缺陷

Async/await 让你的代码看起来是同步的，在某种程度上，也使得它的行为更加地同步。 await关键字会阻塞其后的代码，直到 promise完成，就像执行同步操作一样。它确实可以允许其他任务在此期间继续运行，但您自己的代码被阻塞。

这意味着您的代码可能会因为大量 await的 promises相继发生而变慢。每个 await都会等待前一个完成，而你实际想要的是所有的这些 promises同时开始处理（就像我们没有使用 async/await时那样）。

**缓解此问题**，通过将 Promise对象存储在变量中来同时开始它们，然后等待它们全部执行完毕。

## 结合 Promise

在 Promise前使用 await可以直接得到该对象 resolve的返回值

```js
async function hello() {
    let greeting = await Promise.resolve("Hello")
  	alert(greeting) // 异步获取抛出的值
  	alert(Promise.resolve("Hello")) // 立即(同步)返回一个 Promise对象,需要 .thon获取异步值
    return greeting
};

hello().then(alert); // 异步获取抛出的值

// 一般的使用方式 0.0
const hello1 = () => {

    return new Promise((resolve, reject) => {
        // code
        if(/* 请求成功 */){
            resolve(val)
        }
    })
}

async function gethello(){
    const val = await hello1()
    alert(val)
}


```


参考来源

[阮一峰的网络日志 ](http://www.ruanyifeng.com/blog/2015/05/async.html)

[MDN](https://developer.mozilla.org/zh-CN/docs/learn/JavaScript/%E5%BC%82%E6%AD%A5/Async_await)