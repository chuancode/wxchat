## scroll-view

可滚动视图区域。使用竖向滚动时，需要给scroll-view一个固定高度，通过 WXSS 设置 height。

**属性**

+ `scroll-x`  允许横向滚动，默认 false
+ `scroll-y`  允许竖向滚动, 默认 false
+ `scroll-top/left`  设置竖向/横向 滚动条位置
+ `e-block-to-top` 双击（ios状态栏，安卓标题栏）返回顶部，仅支持竖向。默认 false

## Promise 简单封装 wx.request()

**Promise 是什么：**

Promise是异步编程的一种解决方案，比传统的解决方案——回调函数和事件——更合理和更强大。  

Promise，简单说就是一个容器，里面保存着某个未来才会结束的事件（通常是一个异步操作）的结果。从语法上说，Promise是一个对象，从它可以获取异步操作的消息。Promise提供统一的 API，各种异步操作都可以用同样的方法进行处理。

**Promise**的特点：

1. 对象的状态不受外界影响。Promise对象代表一个异步操作，有三种状态：Pending（进行中）、Resolved（已完成，又称 Fulfilled）和 Rejected（已失败）。只有异步操作的结果，可以决定当前是哪一种状态，任何其他操作都无法改变这个状态。
2. 一旦状态改变，就不会再变，任何时候都可以得到这个结果。Promise对象的状态改变，只有两种可能：从  Pending变为 Resolved和从 Pending变为 Rejected。只要这两种情况发生，状态就凝固了，不会再变了，会一直保持这个结果。就算改变已经发生了，你再对 Promise对象添加回调函数，也会立即得到这个结果。这与事件（Event）完全不同，事件的特点是，如果你错过了它，再去监听，是得不到结果的。

**使用方向：**
多层请求嵌套。可以将异步操作以同步操作的流程表达出来，避免了层层嵌套的回调函数。此外，Promise对象提供统一的接口，使得控制异步操作更加容易。

**缺点：**
1. 无法取消 Promise，一旦新建它就会立即执行，无法中途取消。
2. 如果不设置回调函数，Promise内部抛出的错误，不会反应到外部。
3. 当处于 Pending状态时，无法得知目前进展到哪一个阶段（刚刚开始还是即将完成）。

> 如果某些事件不断地反复发生，一般来说，使用stream模式是比部署Promise更好的选择。

简单示例：

```js
var promise = new Promise((resolve, reject)=>{
    // code
    if(/* 判断请求状态 */) {
        resolve(val)
    } else {
        reject(err)
    }
})
```

**解释：**

Promise构造函数接受一个函数作为参数，该函数的两个参数分别是 resolve和 reject。它们是两个函数，由JavaScript引擎提供，不用自己部署。

resolve函数的作用是，将 Promise对象的状态从“未完成”变为“成功”（即从 Pending变为 Resolved），在异步操作成功时调用，并将异步操作的结果，作为参数传递出去；

reject函数的作用是，将 Promise对象的状态从“未完成”变为“失败”（即从 Pending变为 Rejected），在异步操作失败时调用，并将异步操作报出的错误，作为参数传递出去。

---

### Promise.then
**Promise实例生成以后**, 可以用 then方法分别指定 Resolved状态和 Reject状态的回调函数。

```js
promise.then(val => {
    // success
}, err => {
    // failure
})
```

then方法可以接受两个回调函数作为参数。第一个回调函数是 Promise对象的状态变为 Resolved时调用，第二个回调函数是 Promise对象的状态变为Reject时调用。其中，第二个函数是可选的，不一定要提供。这两个函数都接受 Promise对象传出的值作为参数。


**then方法返回的是一个新的 Promise实例**（注意，不是原来那个Promise实例）。因此可以采用链式写法，即 then方法后面再调用另一个 then方法。

`promise.then().then(val=>{}, err=>{})`

> 第一个 then完成以后，会将放回结果作为参数传入第二个 then的回调函数。如果前一个 回调函数的返回值还是一个 Promise对象。这时后一个回调函数，就会等待该 Promise对象的状态发生变化，才会被调用，同时接受一个 then 对象的状态，如果是 Resolved, 则调用 then的参数 1。否则调用参数2（Rejected。

---

### 简单封装 wx.request()

```js
const request= (params) =>{
    return new Promise((resolve, reject)=>{
        wx.request({

            ...params,
            
            success:(val)=>{
                resolve(val)
            },
            fail:(err)=>{
                reject(err)
            }
        });
    })
}
```

使用

```js
request({
            url: "api/xx"
        })
            .then(val => {
                // 前一个请求成功执行这一个请求, 失败无任何操作
                request({url: val.url})
            })
            .then(
                val2 => {           
                    // val 回调中的请求成功执行 val2         
                    console.log(val2)
                }, err => {
                    // val 回调中的请求失败执行 err
                    console.log("请求服务器错误")
                }
            )
    },
```

---

参考来源

[前端博客 -更加详细完整](http://caibaojian.com/es6/promise.html)

[黑马程序员](https://www.bilibili.com/video/BV1nE41117BQ?p=66)